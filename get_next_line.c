/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_next_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 10:01:16 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/26 11:33:56 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		pos_of_first_c(const char *s, char c)
{
	int	i;

	i = -1;
	if (!s)
		return (ERROR);
	while (s[++i] && s[i] != c)
		;
	if (!s[i])
		return (ERROR);
	return (i);
}

t_files	*add_files(t_files **f, int fd)
{
	t_files	*next;
	t_files	*buff;

	if ((buff = *f))
	{
		while (buff->next)
		{
			if (buff->fd == fd)
				return (buff);
			buff = buff->next;
		}
		if (buff->fd == fd)
			return (buff);
	}
	if (!(next = (t_files *)malloc(sizeof(t_files))) ||
		!(next->buff = ft_strnew(BUFF_SIZE + 1)) ||
		!(next->rest_of_buff = ft_strnew(1)))
		return (NULL);
	next->fd = fd;
	next->eof = FALSE;
	next->next = NULL;
	if (*f && (buff->next = next))
		return (next);
	*f = next;
	return (*f);
}

char	reading(t_files *f)
{
	char	*buff;

	if (!f->eof)
		f->line = ft_strdup(f->rest_of_buff);
	ft_strdel(&f->rest_of_buff);
	f->bytes_readed = -2;
	while (pos_of_first_c(f->line, '\n') < 0 &&
			(f->bytes_readed = read(f->fd, f->buff, BUFF_SIZE)) > 0)
	{
		f->buff[f->bytes_readed] = '\0';
		buff = ft_strdup(f->line);
		ft_strdel(&f->line);
		f->line = ft_insert_at(buff, f->buff, -1);
		ft_strdel(&buff);
	}
	if (f->bytes_readed == ERROR)
		return (ERROR);
	return (OK);
}

int		get_next_line(int const fd, char **line)
{
	static t_files	*files;
	t_files			*f;

	if (!line || BUFF_SIZE < 0 || read(fd, NULL, 0) == -1)
		return (ERROR);
	f = add_files(&files, fd);
	if (reading(f) == ERROR)
		return (ERROR);
	f->pos_of_first_nl = pos_of_first_c(f->line, '\n');
	if (f->pos_of_first_nl >= 0 && !f->eof)
	{
		f->rest_of_buff = ft_strdup(f->line + f->pos_of_first_nl + 1);
		f->buff_ = ft_strsub(f->line, 0, f->pos_of_first_nl);
		ft_strdel(&f->line);
		f->line = ft_strdup(f->buff_);
		ft_strdel(&f->buff_);
	}
	*line = ft_strdup(f->line);
	if ((f->bytes_readed == 0 || f->bytes_readed == -2) &&
			!(f->rest_of_buff) && ft_strlen(f->line) == 0)
		return (END_OF_FILE);
	if (ft_strlen(*line) == 0 && f->eof)
		return (END_OF_FILE);
	ft_strdel(&f->line);
	return (OK);
}
