/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 17:14:42 by Belotte           #+#    #+#             */
/*   Updated: 2015/12/26 12:05:27 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t len)
{
	size_t			i;
	unsigned char	*str1;
	unsigned char	*str2;
	void			*buf;

	i = -1;
	str1 = (unsigned char *)dest;
	str2 = (unsigned char *)src;
	while (++i < len)
		str1[i] = str2[i];
	buf = (void *)str1;
	return (buf);
}
