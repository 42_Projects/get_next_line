/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rand.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/28 10:36:23 by fbellott          #+#    #+#             */
/*   Updated: 2015/12/28 11:07:46 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_rand(int min, int max)
{
	char	*c;
	char	*d;
	char	*e;
	int		n;

	c = (char *)malloc(1);
	d = (char *)malloc(1);
	e = (char *)malloc(1);
	n = (unsigned int)c * (unsigned int)d * (unsigned int)e /
		100 % (max - min + 1) + min;
	return (n);
}
