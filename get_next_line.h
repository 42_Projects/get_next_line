/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 11:40:51 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/07 15:26:00 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <unistd.h>
# include <stdlib.h>
# include "libft.h"

# define BUFF_SIZE		100

# define OK				1
# define END_OF_FILE	0
# define ERROR			-1

# define TRUE			1
# define FALSE			0

typedef struct			s_files
{
	int					fd;
	char				*buff;
	char				*buff_;
	char				*line;
	char				*rest_of_buff;
	int					flag;
	int					bytes_readed;
	int					pos_of_first_nl;
	char				eof;
	struct s_files		*next;
}						t_files;

int						get_next_line(int const fd, char **line);

#endif
