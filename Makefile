NAME	= gnl
CC		= gcc
FT_PATH	= libft
S_DIR	= ./
O_DIR	= objs/
I_DIR	= -I . -I ./$(FT_PATH)/includes
CFLAGS	= -g -Wall -Wextra -Werror $(I_DIR) $(LIBS_DIR)
FILES	= get_next_line.c
LIBS_F	= -L./$(FT_PATH) -lft
SRC		= $(addprefix $(S_DIR),$(FILES))
OBJS	= $(addprefix $(O_DIR),$(FILES:.c=.o))
RM		= rm -f
COUNTER	= 1

all:		start $(NAME)

$(NAME): 	$(OBJS) $(O_DIR)main.o
	@echo ""
	@echo ""
	@$(CC) $(LIBS_F) $^ -o $@ $(CFLAGS)
	@echo "./[0;34m$(NAME)[0;38m created."
	@tput cnorm

$(O_DIR)%.o:		$(S_DIR)%.c
	@$(CC) -c $< -o $@ $(CFLAGS)
	@echo -n '.'

$(O_DIR)main.o: main.c
	@$(CC) -c $< -o $@ $(CFLAGS)
	@echo -n '.'

clean:
	@make -C $(FT_PATH) clean
	@$(RM) $(OBJS) $(O_DIR)main.o

fclean: 	clean
	@$(RM) $(NAME)
	@$(RM) $(FT_PATH)/libft_light.a
	@echo "[0;1mClear."

re:			fclean all

start:
	@tput civis
	@clear
	@echo "                     [0;34m  __  __       _        _____ _ _      "
	@echo '                      |  \/  | __ _| | _____|  ___(_) | ___ '
	@echo '                      | |\/| |/ _` | |/ / _ \ |_  | | |/ _ \'
	@echo '                      | |  | | (_| |   <  __/  _| | | |  __/'
	@echo '                      |_|  |_|\__,_|_|\_\___|_|   |_|_|\___|'
	@echo ""
	@make -C $(FT_PATH)
	@echo ""
	@echo ""
	@echo "[0;38mCompiling $(NAME)"
	@echo "[0;31m["
	@tput cup 11 $$(($(COUNTER) + 2))
	@echo "][0;36m"
	@tput cup 11 1

.PHONY: re clean fclean all
