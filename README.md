# Get_Next_Line

A function that read a file line by line. Support multiple file descriptor.
```
#include <fcntl.h>
#include "get_next_line.h"

int			main(int ac, char **av)
{
	char	*line;
	int		fd;
	if (ac <= 0 || (fd = open (av[1], O_RDONLY)) < 0)
	{
		return (1);
	}
	while ((get_next_line (fd, &line)) > 0)
	{
		ft_putstr(line);
		ft_putchar('\n');
		ft_strdel(&line);
	}
	return (0);
}
```